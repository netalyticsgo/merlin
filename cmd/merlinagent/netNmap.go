// gonmap - Wrapper around Nmap
// Copyright (c) 2017, 2018, DCSO GmbH

// Package gonmap port scanning is implemented through the PortScan type.
package main
import (
	"bytes"
	"encoding/xml"
	"fmt"
	"os/exec"
	"github.com/timest/gomanuf"
)

const (
	nmapBin = "nmap"
)

type Nmap interface {
	Result() NmapRun
	Run() error
	Target() string
}

type NmapRunState struct {
	XMLName   xml.Name `xml:"state"`
	State     string   `xml:"state,attr"`
	Reason    string   `xml:"reason,attr"`
	ReasonTTL int      `xml:"reason_ttl,attr"`
}

type NmapRunStatus struct {
	XMLName   xml.Name `xml:"status"`
	State     string   `xml:"state,attr"`
	Reason    string   `xml:"reason,attr"`
	ReasonTTL int      `xml:"reason_ttl,attr"`
}

type NmapRunPortService struct {
	XMLName xml.Name `xml:"service"`
	Name    string   `xml:"name,attr"`
	Method  string   `xml:"method,attr"`
	Conf    int      `xml:"conf,attr"`
}

type NmapRunPort struct {
	XMLName  xml.Name           `xml:"port"`
	Protocol string             `xml:"protocol,attr"`
	Port     int                `xml:"portid,attr"`
	Status   NmapRunState       `xml:"state"`
	Service  NmapRunPortService `xml:"service"`
}

type NmapRunHostName struct {
	XMLName xml.Name `xml:"address"`
	Address string   `xml:"addr,attr"`
	Type    string   `xml:"addrtype,attr"`
}

type NmapRunHostAddress struct {
	XMLName xml.Name `xml:"address"`
	Address string   `xml:"addr,attr"`
	Type    string   `xml:"addrtype,attr"`
}

type NmapRunHost struct {
	XMLName xml.Name           `xml:"host"`
	Ports   []NmapRunPort      `xml:"ports>port"`
	Address []NmapRunHostAddress `xml:"address"`
	Status  NmapRunStatus      `xml:"status"`
}

type NmapRunStatsFinished struct {
	XMLName xml.Name `xml:"finished"`
	Time    int64    `xml:"time,attr"`
	Elapsed float32  `xml:"elapsed,attr"`
}

type NmapRunStats struct {
	XMLName  xml.Name             `xml:"runstats"`
	Finished NmapRunStatsFinished `xml:"finished"`
}

type NmapRun struct {
	XMLName xml.Name      `xml:"nmaprun"`
	Version string        `xml:"version"`
	Args    string        `xml:"args,attr"`
	Start   int64         `xml:"start,attr"`
	Hosts   []NmapRunHost `xml:"host"`
	Stats   NmapRunStats  `xml:"runstats"`
}
// portScanProtocols are the protocols we support when doing port scans
// with Nmap. The values in the map represent the Nmap options.


// PortScan holds information for running the port scan and provides
// functionality to run and get the result.
type PortScan struct {
	target    string
	protocols map[string]struct{}
	result    NmapRun
	Nmap
}

// NewPortScan creates a new PortScan using a target and protocols. A target
// can be either an IP address or a hostname. `protocols` should be a slice
// of strings containing 'tcp' or 'udp' or both.
func NewPortScan(target string, protocols []string) (*PortScan, error) {
	n := &PortScan{
		target:    target,
		protocols: make(map[string]struct{}),
	}
	err := n.SetProtocols(protocols)
	if err != nil {
		return nil, err
	}
	return n, nil
}

// Target returns the target.
func (n *PortScan) Target() string {
	return n.target
}

// Protocols returns a slice of strings containing the protocols used for
// performing the port scan.
func (n *PortScan) Protocols() []string {
	r := []string{}
	for p := range n.protocols {
		r = append(r, p)
	}
	return r
}

// SetProtocols sets the protocol or protocols for performing the port scan.
func (n *PortScan) SetProtocols(protocols []string) error {
	// List of supported protocols

	for _, p := range protocols {
		n.protocols[p] = struct{}{}
	}
	return nil
}

// unmarschalXML calls `xml.Unmarshal` to process the result of Nmap. The result
// is stored.
func (n *PortScan) unmarschalXML(xmldoc []byte) error {
	return xml.Unmarshal(xmldoc, &n.result)
}

// Run executes the port scan. The result is stored and can be retrieved using
// the `Result()` function.
func (n *PortScan) Run() error {
	cmdArgs := []string{"-oX", "-"}
	for p := range n.protocols {
		cmdArgs = append(cmdArgs, p)
	}
	cmdArgs = append(cmdArgs, n.target)
	fmt.Println("Nmap Commands: ", cmdArgs)
	cmd := exec.Command(nmapBin, cmdArgs...)
	var stdout bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Run()
	// nmap does not set exit code on scan failures

	n.unmarschalXML(stdout.Bytes())
	if len(n.result.Hosts) == 0 {
		// if host is not mentioned in XML, it means we could not use it
		return fmt.Errorf("failed scanning target '%s'", n.target)
	}
	return nil
}

// Result returns the results after running the port scan.
func (n *PortScan) Result() NmapRun {
	return n.result
}

func (n *PortScan) FormatResult() {

}

type ports struct {
	Product string `json:"product"`
	State string `json:"state"`
	Name string `json:"name"`
}

type address struct {
	IpAddress string `json:"ipAddress"`
	MacAddress string `json:"macAddress"`
	Vendor string `json:"vendor"`
}

type scanData struct {
	Ports map[string]map[int]ports `json:"ports"`
	Address address `json:"address"`
}

type nmapData map[string]scanData


func getResults(scan *PortScan) (nmapData) {
	nmapScanData := make(nmapData, 0)
	for _, host := range scan.Result().Hosts {
		//fmt.Println(host.XMLName.Space)
		ipAddress := ""
		macAddress := ""
		for _, address := range host.Address {
			if address.Type == "mac" {
				macAddress = address.Address
			} else {
				ipAddress = address.Address
			}
		}
		if len(ipAddress) == 0 || len(macAddress) == 0{
			continue
		}
		Address := address{ipAddress, macAddress, manuf.Search(macAddress)}
		Ports := make(map[string]map[int]ports)
		for _, p := range host.Ports {
			protocol := p.Protocol
			if _, ok := Ports[protocol]; ok {

			} else {
				Ports[protocol] = make(map[int]ports)
			}
			Ports[protocol][p.Port] = ports{"", p.Status.State, p.Service.Name}
		}
		nmapScanData[ipAddress] = scanData{Ports, Address}
	}
	//fmt.Println(nmapScanData)
	return nmapScanData
}
