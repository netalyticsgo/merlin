package main

import (
	"net"
	"fmt"
	"strings"
	"math"
	"strconv"
	"encoding/hex"
	"unicode"
)

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func resolveHostIp() (string) {
	netInterfaceAddresses, err := net.InterfaceAddrs()
	if err != nil { return "" }
	for _, netInterfaceAddress := range netInterfaceAddresses {
		networkIp, ok := netInterfaceAddress.(*net.IPNet)
		if ok && !networkIp.IP.IsLoopback() && networkIp.IP.To4() != nil {
			ip := networkIp.IP.String()
			fmt.Println("Resolved Host IP: " + ip)
			return ip
		}
	}
	return ""
}

func getHostCidrRange() string {
	cidrIp := ""
	localIp := resolveHostIp()
	if len(localIp) == 0 {
		return ""
	}
	ip := strings.Split(localIp, ".")
	ip[len(ip) - 1] = "0"
	cidrIp = strings.Join(ip, ".") + "/24"
	return cidrIp
}

func fieldConvert(conv string, v interface{}) (interface{}, error) {
	if conv == "" {
		if bs, ok := v.([]byte); ok {
			return string(bs), nil
		}
		return v, nil
	}

	var d int
	if _, err := fmt.Sscanf(conv, "float(%d)", &d); err == nil || conv == "float" {
		switch vt := v.(type) {
		case float32:
			v = float64(vt) / math.Pow10(d)
		case float64:
			v = float64(vt) / math.Pow10(d)
		case int:
			v = float64(vt) / math.Pow10(d)
		case int8:
			v = float64(vt) / math.Pow10(d)
		case int16:
			v = float64(vt) / math.Pow10(d)
		case int32:
			v = float64(vt) / math.Pow10(d)
		case int64:
			v = float64(vt) / math.Pow10(d)
		case uint:
			v = float64(vt) / math.Pow10(d)
		case uint8:
			v = float64(vt) / math.Pow10(d)
		case uint16:
			v = float64(vt) / math.Pow10(d)
		case uint32:
			v = float64(vt) / math.Pow10(d)
		case uint64:
			v = float64(vt) / math.Pow10(d)
		case []byte:
			vf, _ := strconv.ParseFloat(string(vt), 64)
			v = vf / math.Pow10(d)
		case string:
			vf, _ := strconv.ParseFloat(vt, 64)
			v = vf / math.Pow10(d)
		case nil:
			v = float64(0)
		}
		if v == nil {
			v = float64(0)
		}
		return v, nil
	}

	if conv == "int" || conv == "Long"  {
		switch vt := v.(type) {
		case float32:
			v = int64(vt)
		case float64:
			v = int64(vt)
		case int:
			v = int64(vt)
		case int8:
			v = int64(vt)
		case int16:
			v = int64(vt)
		case int32:
			v = int64(vt)
		case int64:
			v = int64(vt)
		case uint:
			v = int64(vt)
		case uint8:
			v = int64(vt)
		case uint16:
			v = int64(vt)
		case uint32:
			v = int64(vt)
		case uint64:
			v = int64(vt)
		case []byte:
			v, _ = strconv.Atoi(string(vt))
		case string:
			v, _ = strconv.Atoi(vt)
		case nil:
			v = int64(0)
		}
		if v == nil {
			v = int64(0)
		}
		return v, nil
	}

	if conv == "double"  {
		switch vt := v.(type) {
		case float32:
			v = float64(vt)
		case float64:
			v = vt
		case int:
			v = float64(vt)
		case int8:
			v = float64(vt)
		case int16:
			v = float64(vt)
		case int32:
			v = float64(vt)
		case int64:
			v = float64(vt)
		case uint:
			v = float64(vt)
		case uint8:
			v = float64(vt)
		case uint16:
			v = float64(vt)
		case uint32:
			v = float64(vt)
		case uint64:
			v = float64(vt)
		case []byte:
			v, _ = strconv.ParseFloat(string(vt), 64)
		case string:
			v, _ = strconv.ParseFloat(vt, 64)
		case nil:
			v = float64(0)
		}
		if v == nil {
			v = float64(0)
		}
		return v, nil
	}

	//if conv == "hwaddr" {
	//	fmt.Println(v)
	//	switch vt := v.(type) {
	//	case string:
	//		v = net.HardwareAddr(vt).String()
	//	case []byte:
	//		v = net.HardwareAddr(vt).String()
	//	default:
	//		return nil, fmt.Errorf("invalid type (%T) for hwaddr conversion", v)
	//	}
	//	return v, nil
	//}

	if conv == "ipaddr" {
		var ipbs []byte

		switch vt := v.(type) {
		case string:
			ipbs = []byte(vt)
		case []byte:
			ipbs = vt
		case nil:
			return "", nil
		default:
			return "", fmt.Errorf("invalid type (%T) for ipaddr conversion", v)
		}

		switch len(ipbs) {
		case 4, 16:
			v = net.IP(ipbs).String()
		default:
			return "", fmt.Errorf("invalid length (%d) for ipaddr conversion", len(ipbs))
		}

		return v, nil
	}

	if conv == "String"{
		if v != nil {
			switch vt := v.(type) {
			case []byte:
				//bytes := v.([]byte)
				return Join(len(vt), vt), nil
			case nil:
				return "", nil
			default:
				return fmt.Sprintf("%s", vt), nil
			}
		}
		return "", nil
	}

	if conv == "hwaddr" {
		if v != nil {
			switch vt := v.(type) {
			case []byte:
				if len(vt) != 6 {
					return fieldConvert(conv, fmt.Sprintf("%s", vt))
				}
				return Join(len(vt), vt), nil
			case string:
				if strings.Contains(vt, ":") {
					return fmt.Sprintf("%s", vt), nil
				} else {
					data, err := hex.DecodeString(vt)
					if err != nil {
						return fmt.Sprintf("%s", vt), nil
					}
					return Join(len(data), data), nil
				}
			case nil:
				return "", nil
			default:
				return fmt.Sprintf("%s", vt), nil
			}
		}
	}

	return "", nil
}

func stripSpaces(str string) string {
	return strings.Map(func(r rune) rune {
		if unicode.IsSpace(r) {
			// if the character is a space, drop it
			return -1
		}
		// else keep it in the string
		return r
	}, str)
}