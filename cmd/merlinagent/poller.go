package main

import (
	"fmt"
	"strings"
	"sync"
	"time"

	"github.com/imroc/req"
	g "github.com/soniah/gosnmp"
)

func deviceWalk(gs *g.GoSNMP, oidInfo Csvstructure, output MapTable) {
	//defer wg.Done()
	base := false
	var result []g.SnmpPDU
	oidSplit := strings.Split(oidInfo.OID, ".")
	if oidSplit[len(oidSplit)-1] == "0" {
		base = true
		oids := []string{oidInfo.OID}
		result1, err2 := gs.Get(oids)
		if err2 != nil {
			fmt.Printf("Get() err: %v OID: %s", err2, oidInfo.OID)
			return
		}
		result = result1.Variables
	} else {
		result1, err2 := gs.BulkWalkAll(oidInfo.OID)
		if err2 != nil {
			fmt.Printf("GetBulk() err: %v for element %s For Oid : %s IP: %s\n", err2, result, oidInfo.OID, gs.Target)
			return
		}
		result = result1
	}

	sep := "." + oidInfo.OID + "."
	if _, ok := output[oidInfo.MetricFamily]; ok {
	} else {
		output[oidInfo.MetricFamily] = make(map[string]intData, 0)
	}

	for val := 0; val < len(result); val++ {
		s := strings.Split(result[val].Name, sep)
		if base == false && len(s) < 2 {
			continue
		}

		indexValue := "0"
		if base == false {
			indexValue = fmt.Sprintf("%s", s[1])
		}
		if _, ok := output[oidInfo.MetricFamily][indexValue]; ok {
		} else {
			output[oidInfo.MetricFamily][indexValue] = make(map[string]interface{})
			output[oidInfo.MetricFamily][indexValue]["indexkey"] = indexValue
		}
		switch result[val].Type {
		case g.OctetString:
			value, _ := fieldConvert(oidInfo.Type, result[val].Value)
			output[oidInfo.MetricFamily][indexValue][oidInfo.Attribute] = value
			//bytes := result[val].Value.([]byte)
			//output[oidInfo.MetricFamily][indexValue][oidInfo.Attribute] = Join(len(bytes), bytes)
		case g.ObjectIdentifier:
			output[oidInfo.MetricFamily][indexValue][oidInfo.Attribute] = result[val].Value
		case g.IPAddress:
			value, _ := fieldConvert("ipaddr", result[val].Value)
			output[oidInfo.MetricFamily][indexValue][oidInfo.Attribute] = value
		default:
			value, _ := fieldConvert(oidInfo.Type, result[val].Value)
			output[oidInfo.MetricFamily][indexValue][oidInfo.Attribute] = value
		}
	}
}

func getTableData(target string, macAddress string, SnmpCreds SnmpDetails, csvData []Csvstructure, output PollData, wg *sync.WaitGroup, isPoller bool) {
	defer wg.Done()
	if len(SnmpCreds.CommunityString) == 0 {
		return
	}
	device := &g.GoSNMP{
		Target:    target,
		Port:      161,
		Community: SnmpCreds.CommunityString,
		Version:   SnmpCreds.Version,
		Timeout:   time.Duration(5) * time.Second,
		MaxOids:   60,
	}
	for count := 0; count < 3; count++ {
		if device.Connect() == nil {
			break
		}
		time.Sleep(2)
	}
	defer device.Conn.Close()

	oidOutput := MapTable{}
	for _, oids := range csvData {
		if (isPoller == true && oids.Polled == false) || (isPoller == false && oids.Baseline == false) {
			continue
		}
		deviceWalk(device, oids, oidOutput)
	}
	tabledata := make(tableData)
	for key, value := range oidOutput {
		if _, ok := tabledata[key]; ok {
		} else {
			tabledata[key] = make([]intData, 0)
		}
		for _, v1 := range value {
			if key == "CPU" {
				if _, ok := v1["cpmCPUMonInterval"]; ok {
					//do something here
				} else {
					continue
				}
				//cpmCPUMonInterval
			}
			if key == "PhysicalMemory" {
				if _, ok := v1["memSize"]; ok {
					//do something here
				} else {
					continue
				}
				//cpmCPUMonInterval
			}
			tabledata[key] = append(tabledata[key], v1)
		}
	}
	// output[target] = tabledata

	if isPoller == false {
		payloadData := make(map[string]interface{})
		payloadData["snmpCredentials"] = SnmpCreds
		payloadData["ipAddress"] = target
		payloadData["macAddress"] = macAddress
		for k, v := range tabledata {
			payloadData[k] = v
		}

		// posting to mongodb
		payload := req.BodyJSON(&payloadData)
		header := req.Header{
			"Content-Type": "application/json",
		}
		fmt.Println(req.Post("https://dev.netalytics.tech/api/network_devices", header, payload))
	} else {
		Inventory := PollData{}
		Inventory[target] = tabledata
		InfluxDb(Inventory)
	}

	// f, _ := os.Create("/tmp/" + target + ".json")
	// data, _ := json.Marshal(tabledata)
	// f.Write(data)
	// f.Close()
}
