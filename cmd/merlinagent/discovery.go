package main

import (
	"bufio"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/Knetic/govaluate"
	"github.com/imroc/req"
	g "github.com/soniah/gosnmp"
	c "github.com/udhos/jazigo/conf"
	d "github.com/udhos/jazigo/dev"
	"github.com/udhos/jazigo/temp"
)

var mu sync.Mutex
var deviceList = make(map[string]*DeviceHolder)

var rules RuleGen

func buildDevice(ipaddress string) *DeviceHolder {
	device := &g.GoSNMP{
		Target:    ipaddress,
		Port:      161,
		Community: "public",
		Version:   g.Version2c,
		Timeout:   time.Duration(1) * time.Second,
		MaxOids:   60,
	}
	devDetails := DeviceDetails{"", "", "", "", 0, "", 0}
	ret := DeviceHolder{ipaddress, device, make(map[string]ConnectedDevice), make(map[string]string), DeviceInterfaces{make(map[int]map[string]string)}, []string{}, []string{}, SnmpDetails{}, devDetails, TransportCredentials{}}
	deviceList[ipaddress] = &ret
	return &ret
}

func getTransportCredentials() *TransportCredentialsList {
	cred1 := TransportCredentials{}
	cred1.Type = "ssh"
	cred1.Port = 22
	cred1.UserName = "cisco"
	cred1.Password = "cisco"

	cred2 := TransportCredentials{}
	cred2.Type = "ssh"
	cred2.Port = 22
	cred2.UserName = "cisco"
	cred2.Password = "admin@123"

	tc := []TransportCredentials{}
	tcl := TransportCredentialsList{tc}
	tcl.transcred = append(tcl.transcred, cred1)
	tcl.transcred = append(tcl.transcred, cred2)
	return &tcl
}

func getDiscoveryCredentials() *CredentialList {
	//This will replaced by a call to the server and fetching the details. Remove this comment once done
	cred1 := SnmpDetails{}
	cred1.CommunityString = "public"
	cred1.Version = g.Version2c

	cred2 := SnmpDetails{}
	cred2.CommunityString = "netalytics"
	cred2.Version = g.Version2c

	cred3 := SnmpDetails{}
	cred3.CommunityString = "private"
	cred3.Version = g.Version2c

	snmp := []SnmpDetails{}
	cl := CredentialList{snmp}
	cl.snmp = append(cl.snmp, cred1)
	cl.snmp = append(cl.snmp, cred2)
	cl.snmp = append(cl.snmp, cred3)
	return &cl
}

func getSeedDevices() []string {
	seedDevice := []string{"192.168.31.32", "192.168.31.142", "192.168.31.144", "192.168.31.99", "192.168.31.37"}
	return seedDevice
}

func runNmapScan(nmapdata *nmapData, wg *sync.WaitGroup) {
	defer wg.Done()
	cidrIp := getHostCidrRange()
	if len(cidrIp) == 0 {
		return
	}
	scan, err := NewPortScan(cidrIp, []string{"-sP", "-PR", "-T3"})
	if err != nil {
		return
	}
	scan.Run()
	*nmapdata = getResults(scan)
}

func startdiscovery() {
	var wg sync.WaitGroup
	nmapdata := make(nmapData, 0)
	wg.Add(1)
	go runNmapScan(&nmapdata, &wg)
	cl := getDiscoveryCredentials()
	seedDevice := getSeedDevices()
	snmpchannel := make(chan *DeviceHolder, 0)
	output := make(chan *DeviceHolder, 0)
	//connectedDeviceChannel:=make(chan *DeviceHolder,10)
	for _, x := range seedDevice {
		go DiscoverCommunityString(snmpchannel, output, *cl)
		snmpchannel <- buildDevice(x)
	}
	runDiscovery(snmpchannel, output, *cl, &wg, seedDevice)
	wg.Wait()
	reportData := make([]DiscoveryData, 0)

	count := int(0)
	for ipAddress, device := range deviceList {
		discoveryData := DiscoveryData{}
		discoveryData.IpAddress = ipAddress
		if _, ok := nmapdata[ipAddress]; ok {
			discoveryData.MacAddress = nmapdata[ipAddress].Address.MacAddress
		}
		discoveryData.SnmpDiscovered = false
		if len(device.Device.Community) != 0 {
			count += 1
			discoveryData.SnmpDiscovered = true
			discoveryData.DeviceDetails = device.DeviceDetails
			discoveryData.SnmpCreds = device.SnmpDetails
			discoveryData.MatchedRules = device.MatchedRules
		}
		if discoveryData.SnmpDiscovered == true {
			reportData = append(reportData, discoveryData)
		}
	}
	for ipAddress, details := range nmapdata {
		discoveryData := DiscoveryData{}
		if _, ok := deviceList[ipAddress]; ok {
			continue
		}
		discoveryData.IpAddress = ipAddress
		discoveryData.MacAddress = details.Address.MacAddress
		discoveryData.SnmpDiscovered = false
		reportData = append(reportData, discoveryData)
	}
	f, _ := os.Create("/tmp/dat2.json")
	data, _ := json.Marshal(reportData)
	f.Write(data)
	f.Close()
	fmt.Println("Total Discovered Devices: ", count)
	fmt.Println("TOTAL DEVICES: ", len(reportData))
	//return
	var csvData []Csvstructure
	csvFile, _ := os.Open("/home/ram/go/src/github.com/Ne0nd0g/merlin/cmd/merlinagent/commonDiscovery.csv")
	reader := csv.NewReader(bufio.NewReader(csvFile))
	count = 0
	for {
		line, error := reader.Read()
		if error == io.EOF {
			break
		} else if error != nil {
			log.Fatal(error)
		}
		if count == 0 {
			count += 1
			continue
		}
		count += 1
		baseline, _ := strconv.ParseBool(line[4])
		polled, _ := strconv.ParseBool(line[5])
		csvData = append(csvData, Csvstructure{
			OID:          stripSpaces(line[0]),
			MetricFamily: stripSpaces(line[1]),
			Metric:       stripSpaces(line[2]),
			Type:         line[3],
			Baseline:     baseline,
			Polled:       polled,
			Units:        line[6],
			VendorCert:   line[7],
			Attribute:    stripSpaces(line[8]),
		})
	}

	// Login to the server
	header := req.Header{
		"Content-Type": "application/json",
	}
	content := login{
		Username: "venuthepower@gmail.com",
		Password: "admin@123",
	}
	payload := req.BodyJSON(&content)
	_, err := req.Post("https://dev.netalytics.tech/api/login", header, payload)
	if err != nil {
		fmt.Println(err)
	}

	Inventory := PollData{}
	for _, data := range reportData {
		if data.SnmpDiscovered == true {
			wg.Add(1)
			go getTableData(data.IpAddress, data.MacAddress, data.SnmpCreds, csvData, Inventory, &wg, false)
		}
	}
	wg.Wait()
	// InfluxDb(Inventory)
}

func runDiscovery(snmpchannel chan *DeviceHolder, output chan *DeviceHolder, credentialstoTry CredentialList, wg *sync.WaitGroup, seedDevices []string) {
	for val := 0; val < len(seedDevices); val++ {
		values := <-output
		if len(values.Device.Community) == 0 {
			continue
		}
		fmt.Println("Community Discovered for " + values.IPorhostName + "::" + values.Device.Community)
		wg.Add(1)
		values.discoverType()
		go values.discoverInterfacesAndNeighbors(snmpchannel, output, credentialstoTry, wg)
	}
}

func Join(length int, value []byte) string {
	sep := string("")
	strs := make([]string, length)
	if length == 4 {
		sep = "."
		for i, v := range value {
			strs[i] = strconv.Itoa(int(v))
		}
	} else if length == 6 {
		sep = ":"
		for i, v := range value {
			strs[i] = fmt.Sprintf("%02x", v)
		}
	} else {
		return fmt.Sprintf("%s", value)
	}
	return strings.Join(strs, sep)
}

func (d *DeviceHolder) processInterfaces(ifvalues []g.SnmpPDU, snmpchannel chan *DeviceHolder) {
	for val := 0; val < len(ifvalues); val++ {
		indexwithvar := strings.Replace(ifvalues[val].Name, "."+"1.3.6.1.2.1.2.2.1", "", 1)[1:]
		curindex := indexwithvar[strings.Index(indexwithvar, "."):][1:]
		currMib, _ := strconv.Atoi(indexwithvar[:strings.Index(indexwithvar, ".")])
		if _, ok := d.Interfaces.interfaces[currMib]; ok {
			//do something here
		} else {
			d.Interfaces.interfaces[currMib] = make(map[string]string)
		}
		switch ifvalues[val].Type {
		case g.OctetString:
			bytes := ifvalues[val].Value.([]byte)
			d.Interfaces.interfaces[currMib][curindex] = Join(len(bytes), bytes)
		default:
			t := fmt.Sprintf("%v", g.ToBigInt(ifvalues[val].Value))
			d.Interfaces.interfaces[currMib][curindex] = t
		}
	}
}

func (d *DeviceHolder) processCDP(cdpvalues []g.SnmpPDU, snmpchannel chan *DeviceHolder) {
	//fmt.Println("In CDP")
	for val := 0; val < len(cdpvalues); val++ {
		ipAddress := string("")
		switch cdpvalues[val].Type {
		case g.OctetString:
			bytes := cdpvalues[val].Value.([]byte)
			ipAddress = Join(len(bytes), bytes)
		default:
			//fmt.Println(cdpvalues[val].Value)
		}
		if len(ipAddress) == 0 {
			continue
		}
		if stringInSlice(ipAddress, d.NeighborIps) {

		} else {
			d.NeighborIps = append(d.NeighborIps, ipAddress)
		}
	}
}

func (d *DeviceHolder) processLLDP(lldpvalues []g.SnmpPDU, snmpchannel chan *DeviceHolder) {
	for val := 0; val < len(lldpvalues); val++ {
		name := lldpvalues[val].Name
		name = strings.Replace(lldpvalues[val].Name, "."+"1.0.8802.1.1.2.1.4.2.1.4.", "", 1)
		name = strings.Join(strings.Split(name, ".")[3:], ".")
		ipAddress := strings.TrimPrefix(name, "1.4.")
		if ipAddress != name {
			if stringInSlice(ipAddress, d.NeighborIps) {

			} else {
				d.NeighborIps = append(d.NeighborIps, ipAddress)
			}
		}
	}
}

func (d *DeviceHolder) processBridgeMIB(bridgevalues []g.SnmpPDU, snmpchannel chan *DeviceHolder) {
	for val := 0; val < len(bridgevalues); val++ {
		ipAddress := string("")
		switch bridgevalues[val].Type {
		case g.OctetString:
			bytes := bridgevalues[val].Value.([]byte)
			ipAddress = Join(len(bytes), bytes)

		case g.IPAddress:
			ipAddress = fmt.Sprintf("%v", bridgevalues[val].Value)
		}
		if len(ipAddress) == 0 {
			continue
		}

		if stringInSlice(ipAddress, d.NeighborIps) {

		} else {
			d.NeighborIps = append(d.NeighborIps, ipAddress)
		}
	}
}

func (dev *DeviceHolder) discoverTransportCredentials(transportCredsToTry TransportCredentialsList, snmpchannel chan *DeviceHolder) {
	device := dev.Device
	fmt.Println("Transport credentials query for:", device.Target)

	logger := log.New(os.Stdout, "", log.LstdFlags)
	tab := d.NewDeviceTable()
	opt := c.NewOptions()
	opt.Set(&c.AppConfig{MaxConcurrency: 3, MaxConfigFiles: 10})
	d.RegisterModels(logger, tab)
	repo := temp.MakeTempRepo()
	errlogPrefix := filepath.Join(repo, "errlog_test.")
	defer temp.CleanupTempRepo()
	requestCh := make(chan d.FetchRequest)
	go d.Spawner(tab, logger, requestCh, repo, errlogPrefix, opt, d.NewFilterTable(logger))
	for _, s := range transportCredsToTry.transcred {
		ipport := device.Target + ":" + strconv.Itoa(s.Port)
		d.CreateDevice(tab, logger, "cisco-ios", ipport, ipport, s.Type, s.UserName, s.Password, s.Enable, false, nil)
		good, bad, skip := d.Scan(tab, tab.ListDevices(), logger, opt.Get(), requestCh)
		if good == 1 && bad == 0 && skip == 0 {
			dev.DeviceTransportCreds = s
			fmt.Println("Worked SSH Credentials for:", device.Target, s)
			break
		}
	}
}

func (d *DeviceHolder) deviceTypeRules(snmpchannel chan *DeviceHolder) {

}

func loadRules() {
	if rules.Rules == nil {
		file, e := ioutil.ReadFile("./config.json")
		if e != nil {
			fmt.Printf("File error: %v\n", e)
			os.Exit(1)
		}
		//fmt.Printf("%s\n", string(file))
		//x:=RuleGen{}
		json.Unmarshal([]byte(string(file)), &rules)
	}
}

func (d *DeviceHolder) discoverInterfacesAndNeighbors(snmpchannel chan *DeviceHolder, output chan *DeviceHolder, credentialstoTry CredentialList, wg *sync.WaitGroup) {
	defer wg.Done()
	device := d.Device
	// tcl := getTransportCredentials()
	fmt.Println("Connected Device Discovery ", device.Target)
	err := device.Connect()
	if err != nil {
		fmt.Printf("Connect() err: %v", err)
		return
	}
	device.Timeout = time.Duration(30) * time.Second
	defer device.Conn.Close()
	//
	oids := []string{"1.3.6.1.2.1.2.2", "1.3.6.1.4.1.9.9.23.1.2.1.1.4", "1.3.6.1.2.1.4.22.1.3", "1.0.8802.1.1.2.1.4.2.1.4"}
	results := make(map[string][]g.SnmpPDU)
	for _, element := range oids {
		//fmt.Println("Querying device %s for %s ", device.Target, element)
		result, err2 := device.BulkWalkAll(element)
		if err2 != nil {
			fmt.Printf("GetBulk() err: %v for element %s", err2, element)
		} else {
			results[element] = result
		}
	}
	d.NeighborIps = make([]string, 0)
	//fmt.Println("IP ADDRESS: %s", device.Target)
	d.processInterfaces(results["1.3.6.1.2.1.2.2"], snmpchannel)
	d.processCDP(results["1.3.6.1.4.1.9.9.23.1.2.1.1.4"], snmpchannel)
	d.processLLDP(results["1.0.8802.1.1.2.1.4.2.1.4"], snmpchannel)
	d.processBridgeMIB(results["1.3.6.1.2.1.4.22.1.3"], snmpchannel)
	d.discoverType()
	// d.discoverTransportCredentials(*tcl, snmpchannel)
	for _, element := range d.NeighborIps {
		if _, ok := deviceList[element]; ok {

		} else {
			go DiscoverCommunityString(snmpchannel, output, credentialstoTry)
			snmpchannel <- buildDevice(element)
			go runDiscovery(snmpchannel, output, credentialstoTry, wg, []string{element})
		}
	}
}

func (d *DeviceHolder) discoverType() {
	//fmt.Println("RULES: ", rules.Rules)
	err := d.Device.Connect()
	if err != nil {
		fmt.Printf("Connect() err: %v", err)
		return
	}
	defer d.Device.Conn.Close()
	for _, rule := range rules.Rules {
		condition := rule.Condition
		re := regexp.MustCompile("snmp_([\\d|_]*)")

		values := re.FindAllString(condition, -1)
		parameters := make(map[string]interface{})
		temp := make(map[string]interface{})
		for _, val := range values {
			temp[strings.Replace(strings.Replace(val, "snmp", "", -1), "_", ".", -1)] = ""
		}
		oids := make([]string, len(temp))
		i := 0
		for k := range temp {
			oids[i] = k[1:]
			i++
		}
		//fmt.Println("OIDS: %s\n", oids)
		//get the oids
		ret, err2 := d.Device.Get(oids)
		if err2 != nil {
			fmt.Errorf("%s", err2)
		}
		for _, val := range ret.Variables {
			name := strings.Replace(val.Name, ".", "_", -1)
			name = "snmp" + name
			//fmt.Printf("string: %s\n", name)
			if val.Type == g.OctetString {
				bytes := val.Value.([]byte)
				parameters[name] = string(bytes)
			} else {
				parameters[name] = val.Value
			}
		}
		re = regexp.MustCompile("regex_([\\w|_]*)")
		values = re.FindAllString(condition, -1)
		for _, val := range values {
			for _, regex := range rule.Regexes {
				if regex.Name == val {
					parameters[val] = regexp.MustCompile(regex.Value)
				}
			}
		}
		expression, err := govaluate.NewEvaluableExpression(rule.Condition)
		if err != nil {
			fmt.Errorf("%s", err)
		}
		result, err := expression.Evaluate(parameters)
		if result == true && !stringInSlice(rule.Rulename, d.MatchedRules) {
			d.MatchedRules = append(d.MatchedRules, rule.Rulename)
		}
	}
}

func NetalyticsDiscovery() {
	loadRules()
	startdiscovery()
	fmt.Println("Completed")
}

func DiscoverConnectedDevices(c chan *DeviceHolder, wg *sync.WaitGroup, snmpchannel chan *DeviceHolder, output chan *DeviceHolder, cl CredentialList) {
	defer wg.Done()
	deviceholder := <-c
	device := deviceholder.Device
	fmt.Println("Connected Device Discovery %s  %s", device.Target, device.Community)
	err := device.Connect()
	if err != nil {
		fmt.Printf("Connect() err: %v", err)
		return
	}
	device.Timeout = time.Duration(30) * time.Second
	defer device.Conn.Close()
	//
	oids := []string{"1.3.6.1.2.1.2.2", "1.3.6.1.4.1.9.9.23.1.2.1.1", "1.3.6.1.2.1.4.22.1", "1.0.8802.1.1.2.1.4.2.1"}
	results := make(map[string][]g.SnmpPDU)
	for _, element := range oids {
		fmt.Println("Querying device %s for %s ", device.Target, element)
		result, err2 := device.BulkWalkAll(element)
		if err2 != nil {
			fmt.Printf("GetBulk() err: %v for element %s", err2, element)
		} else {
			results[element] = result
		}
	}
	_, ok := results["1.3.6.1.2.1.2.2"]
	if ok {
		for _, variable := range results["1.3.6.1.2.1.2.2"] {
			indexwithvar := strings.Replace(variable.Name, "."+"1.3.6.1.2.1.2.2.1", "", 1)[1:]
			curindex := indexwithvar[strings.Index(indexwithvar, "."):][1:]
			ifIndex, _ := strconv.Atoi(curindex)
			var currInterface map[string]string
			if _, ok := deviceholder.Interfaces.interfaces[ifIndex]; ok {
				currInterface = deviceholder.Interfaces.interfaces[ifIndex]
			} else {
				currInterface = make(map[string]string)
				deviceholder.Interfaces.interfaces[ifIndex] = currInterface
			}
		}
	}
	for _, variable := range results["1.3.6.1.4.1.9.9.23.1.2.1.1"] {
		indexwithvar := strings.Replace(variable.Name, "."+oids[1], "", 1)[1:]
		curindex := indexwithvar[strings.Index(indexwithvar, "."):][1:]
		var currNeighbor ConnectedDevice
		if _, ok := deviceholder.Discoveredneighbors[curindex]; ok {
			currNeighbor = deviceholder.Discoveredneighbors[curindex]
		} else {
			currNeighbor = ConnectedDevice{"", "", "", "", "", curindex, make(map[string]string)}
			deviceholder.Discoveredneighbors[curindex] = currNeighbor
		}

		// the Value of each variable returned by Get() implements
		// interface{}. You could do a type switch...
		fmt.Println("Got type", variable.Type)
		if strings.Contains(variable.Name, oids[0]+".4") {
			if strings.Contains(variable.Name, oids[0]+".4") {
				data := variable.Value.([]byte)
				var addr string = ""
				for _, val := range data {
					addr += strconv.Itoa(int(val))
					addr += "."
				}
				sz := len(addr)
				finaladdr := addr[:sz-1]

				go DiscoverCommunityString(snmpchannel, output, cl)
				snmpchannel <- buildDevice(finaladdr)
				currNeighbor.IPorhost = finaladdr
				fmt.Println(variable.Name, finaladdr)
			}

			// ... or often you're just interested in numeric values.
			// ToBigInt() will return the Value as a BigInt, for plugging
			// into your calculations
			if strings.Contains(variable.Name, oids[1]+".3") {
				index := strings.Replace(variable.Name, "."+oids[1]+".3", "", 1)[1:]
				address := index[strings.Index(index, "."):][1:]
				fmt.Println(variable.Name, address)
				go DiscoverCommunityString(snmpchannel, output, cl)
				snmpchannel <- buildDevice(address)
			} else {
				fmt.Println(" %s %d", variable.Name, g.ToBigInt(variable.Value))
			}

		}
	}
	fmt.Println("DiscoverConnectedDevices Completed")
}

func DiscoverCommunityString(dev chan *DeviceHolder, output chan *DeviceHolder, credentialstoTry CredentialList) {
	snmpinfo := <-dev
	device := snmpinfo.Device
	//fmt.Println("Entered Snmp Query for ", device.Target)
	for _, c := range credentialstoTry.snmp {
		device.Community = c.CommunityString
		err := device.Connect()
		if err != nil {
			fmt.Printf("Connect() err: %v", err)
			device.Community = ""
			return
		}

		defer device.Conn.Close()

		//fmt.Println("About to call Snmp Query")
		/*m := DiscoverCommunityString(device)*/

		oids := []string{"1.3.6.1.2.1.1.1.0", "1.3.6.1.2.1.1.2.0", "1.3.6.1.2.1.1.3.0", "1.3.6.1.2.1.1.4.0", "1.3.6.1.2.1.1.5.0", "1.3.6.1.2.1.1.6.0", "1.3.6.1.2.1.1.7.0"}
		result, err2 := device.Get(oids) // Get() accepts up to g.MAX_OIDS
		if err2 != nil {
			if strings.TrimPrefix(err2.Error(), "connection refused") != err2.Error() {
				snmpinfo.Device.Community = ""
				output <- snmpinfo
				return
			}
			//fmt.Printf("Get() err: %v %s\n", err2, device.Target)
			device.Community = ""
			continue
		}
		if device.Community == "" {
			snmpinfo.Device.Community = ""
			output <- snmpinfo
			return
		}
		fmt.Printf("Found community working %s %s\n", device.Community, device.Target)
		for _, variable := range result.Variables {
			//fmt.Printf("%d: oid: %s ", i, variable.Name)
			output := string("")
			//number := int64(0)
			switch variable.Type {
			case g.OctetString:
				bytes := variable.Value.([]byte)
				output = string(bytes)
				//fmt.Printf("string: %s\n", string(bytes))
			case g.ObjectIdentifier:
				//fmt.Println(variable.Value)
				output = variable.Value.(string)
			default:
				// ... or often you're just interested in numeric values.
				// ToBigInt() will return the Value as a BigInt, for plugging
				// into your calculations.
				//output = variable.Value.(string)
				//output = ""
				//number = g.ToBigInt(variable.Value)
				//fmt.Printf("number: %d\n", g.ToBigInt(variable.Value))
				output = g.ToBigInt(variable.Value).String()
			}
			if variable.Name == ".1.3.6.1.2.1.1.1.0" {
				snmpinfo.DeviceDetails.SystemDesc = output
			} else if variable.Name == ".1.3.6.1.2.1.1.2.0" {
				if output[0] == '.' {
					output = output[1:len(output)]
				}
				snmpinfo.DeviceDetails.SystemObjectId = output
			} else if variable.Name == ".1.3.6.1.2.1.1.3.0" {
				snmpinfo.DeviceDetails.SystemUpTime, _ = strconv.ParseInt(output, 10, 64)
			} else if variable.Name == ".1.3.6.1.2.1.1.4.0" {
				snmpinfo.DeviceDetails.SystemContact = output
			} else if variable.Name == ".1.3.6.1.2.1.1.5.0" {
				snmpinfo.DeviceDetails.SystemName = output
			} else if variable.Name == ".1.3.6.1.2.1.1.6.0" {
				snmpinfo.DeviceDetails.SystemLocation = output
			} else if variable.Name == ".1.3.6.1.2.1.1.7.0" {
				snmpinfo.DeviceDetails.SystemServices, _ = strconv.ParseInt(output, 10, 64)
			}
		}
		snmpinfo.SnmpDetails = c
		break
	}
	snmpinfo.Device.Community = device.Community
	//fmt.Println(snmpinfo.DeviceDetails)
	output <- snmpinfo
}
