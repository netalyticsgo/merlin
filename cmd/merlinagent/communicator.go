package main

import (
	"github.com/influxdata/influxdb/client/v2"
	"fmt"
	"time"
	"log"
	"strings"
)

const (
	dbName   = "netalytics"
	username = "netalytics"
	password = "Net4Cisco"
)

func InfluxDb(output PollData) {
	c, err := client.NewHTTPClient(client.HTTPConfig{
		Addr:     "http://35.192.177.129:8086",
		Username: username,
		Password: password,
	})
	if err != nil {
		fmt.Println("Failed to connect to INFLUX")
	} else {
		fmt.Println("Connected to INFLUX")
	}
	defer c.Close()
	// creating a new point batch
	bp, err := client.NewBatchPoints(client.BatchPointsConfig{
		Database:  dbName,
		Precision: "us",
	})
	if err != nil {
		fmt.Println("Error in NewBatchPoints", err)
	}

	for ipAddress, data := range output {
		for family, value := range data {
			// for val := 0; val < len(value); val++ {
			family = strings.Replace(family, " ", "", -1)
			for _, v := range value {
				// Create a point and add to batch
				tags := map[string]string{"hostname": ipAddress, "site": "anirudh"}
				if family == "Interface" {
					if v["ifDescr"] == nil {
						continue
					}
					tags["ifDescr"] = fmt.Sprintf("%s", v["ifDescr"])
				}
				fields := make(map[string]interface{})
				for key, value := range v {
					fields[key] = value
				}
				v["hostname"] = ipAddress
				pt, err := client.NewPoint(family, tags, fields, time.Now())
				if err != nil {
					log.Fatal(err)
				}
				// fmt.Println("ADDING:", pt)
				bp.AddPoint(pt)
			}
		}
	}
	if err := c.Write(bp); err != nil {
		fmt.Println(err)
	}
	fmt.Println(err)
}

