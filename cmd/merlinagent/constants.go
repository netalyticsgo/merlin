package main

import (
	g "github.com/soniah/gosnmp"
)

type InventoryData struct {
	GroupName     string
	PropertyName  string
	PropertyValue string
	OID           string
	Index         []string
}

type login struct {
	Username string `json:"userName"`
	Password string `json:"password"`
}

type SnmpDetails struct {
	Version           g.SnmpVersion `json:"Version"`
	CommunityString   string        `json:"CommunityString"`
	V3UserName        string        `json:"V3UserName"`
	V3AuthProtocol    string        `json:"V3AuthProtocol"`
	V3AuthPassword    string        `json:"V3AuthPassword"`
	V3PrivacyProtocol string        `json:"V3PrivacyProtocol"`
	V3PrivacyPassword string        `json:"V3PrivacyPassword"`
}

type CredentialList struct {
	snmp []SnmpDetails
}

type TransportCredentials struct {
	UserName string
	Password string
	Enable   string
	Port     int
	Type     string
}

type TransportCredentialsList struct {
	transcred []TransportCredentials
}

type DeviceInterfaces struct {
	interfaces map[int]map[string]string
}

type ConnectedDevice struct {
	IPorhost   string
	Sourceport string
	Destport   string
	Deviceid   string
	Macaddress string
	TableIndex string
	Properties map[string]string
}

type DeviceDetails struct {
	SystemName     string `json:"SystemName"`
	SystemDesc     string `json:"SystemDesc"`
	SystemObjectId string `json:"SystemObjectId"`
	SystemContact  string `json:"SystemContact"`
	SystemUpTime   int64  `json:"SystemUpTime"`
	SystemLocation string `json:"SystemLocation"`
	SystemServices int64  `json:"SystemServices"`
}

type DiscoveryData struct {
	IpAddress      string        `json:"ipAddress"`
	MacAddress     string        `json:"macAddress"`
	SnmpDiscovered bool          `json:"SnmpDiscovered"`
	SnmpCreds      SnmpDetails   `json:"SnmpCreds"`
	DeviceDetails  DeviceDetails `json:"DeviceDetails"`
	MatchedRules   []string      `json:"MatchedRules"`
}

type DeviceHolder struct {
	IPorhostName         string
	Device               *g.GoSNMP
	Discoveredneighbors  map[string]ConnectedDevice
	SystemMib            map[string]string
	Interfaces           DeviceInterfaces
	MatchedRules         []string
	NeighborIps          []string
	SnmpDetails          SnmpDetails
	DeviceDetails        DeviceDetails
	DeviceTransportCreds TransportCredentials
}

type RuleGen struct {
	Rules []struct {
		Rulename  string `json:"rulename"`
		Condition string `json:"condition"`
		Regexes   []struct {
			Name  string `json:"name"`
			Value string `json:"value"`
		} `json:"regexes"`
	} `json:"rules"`
}

type Csvstructure struct {
	OID          string `json:"OID"`
	MetricFamily string `json:"MetricFamily"`
	Metric       string `json:"Metric"`
	Type         string `json:"Type"`
	Baseline     bool   `json:"Baseline"`
	Polled       bool   `json:"Polled"`
	Units        string `json:"Units"`
	VendorCert   string `json:"VendorCert"`
	Attribute    string `json:"Attribute"`
}

type InfluxData struct {
	DeviceIp     string
	MetricFamily string
	Metric       string
	Value        string
}

// Creating the map
type intData map[string]interface{}
type MapTable map[string]map[string]intData
type tableData map[string][]intData

type PollData map[string]tableData
